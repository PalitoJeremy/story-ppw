from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def story3_view(request):
	return render(request, "Story3.html")

def about_view(request):
	return render(request, "Story3About.html")

def contact_view(request):
	return render(request, "Story3Contact.html")

def login_view(request):
	return render(request, "Login.html")