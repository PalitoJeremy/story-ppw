from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import story8_view
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
"""
class Lab8UnitTest(TestCase):
	def test_lab8_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab8_using_lab8_func(self):
		found = resolve('/')
		self.assertEqual(found.func, story8_view)

class Lab8FunctionalTest(LiveServerTestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def tearDown(self):
		time.sleep(3)
		self.selenium.quit()
		super(Lab7FunctionalTest, self).tearDown()
"""