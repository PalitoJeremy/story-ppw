from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def story8_view(request):
	return render(request, "Story8.html")