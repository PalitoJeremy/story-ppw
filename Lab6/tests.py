from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from .apps import Lab6Config
from .models import StatusInput
from .forms import StatusForms
from django.apps import apps
from .views import lab6_view
from .views import lab6_profile
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
"""
class Lab6UnitTest(TestCase):
	def test_lab6home_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab6profile_url_is_exist(self):
		response = Client().get('/profile')
		self.assertEqual(response.status_code, 200)

	def test_lab6profile_using_lab6_func(self):
		found = resolve('/profile')
		self.assertEqual(found.func, lab6_profile)

	def test_lab6_using_lab6_func(self):
		found = resolve('/')
		self.assertEqual(found.func, lab6_view)

	def test_model_can_add_new_status(self):
		statusMessage = StatusInput.objects.create(statusMessage='this is a testing')

		counting_all_statusMessage = StatusInput.objects.all().count()
		self.assertEqual(counting_all_statusMessage, 1)

	def test_text_max_length(self):
		status = StatusInput.objects.create(statusMessage = 'LOREM IPSUM LOREM LOREM IPSUM LORLORLOREM EM EM IP IP IPSUM SUM SUM')
		self.assertLessEqual(len(str(status)), 300)

class Lab7FunctionalTest(LiveServerTestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def test_input(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get(self.live_server_url)
		# Find the form element
		statusMessage = selenium.find_element_by_name('statusMessage')
		submit = selenium.find_element_by_id('submit')
		time.sleep(3)

		# Fill the form with data
		statusMessage.send_keys('Coba Coba')

		# Submitting the form
		submit.click()

		self.assertIn("Coba Coba", selenium.page_source)

	def test_title(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		time.sleep(3)
		self.assertIn('Hello!', selenium.title)

	def test_header_with_css_property(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		header_style = selenium.find_element_by_tag_name('h1').value_of_css_property('color')
		time.sleep(3)
		self.assertIn('rgba(252, 252, 252, 1)', header_style)

	def test_body_text_with_css_property(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		result_style = selenium.find_element_by_tag_name('h4').value_of_css_property('color')
		time.sleep(3)
		self.assertIn('rgba(255, 255, 255, 1)', result_style)


	def tearDown(self):
		time.sleep(3)
		self.selenium.quit()
		super(Lab7FunctionalTest, self).tearDown()
"""