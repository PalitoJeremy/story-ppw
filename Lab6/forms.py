from django import forms

class StatusForms(forms.Form):
	errorMessage = {
		'required': 'Please fill the form before submitting.',
	}

	statusMessage = forms.CharField(label='Status', max_length=300, widget=forms.Textarea(attrs={'placeholder': "What's on your mind?", 'class': 'text-field', id:'content-text'}))
# Register your models here.