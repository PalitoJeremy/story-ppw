from django.db import models

# Create your models here.

class StatusInput(models.Model):
	statusMessage = models.CharField(max_length = 300)
	posted = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.statusMessage