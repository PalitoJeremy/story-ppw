from django.db import models

from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', lab6_view, name='home'),
	path('profile', lab6_profile, name='profile'),
	path('add', add_result, name='add')
]