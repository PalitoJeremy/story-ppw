from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForms
from .models import StatusInput

response = {}

# Create your views here.

def lab6_view(request):
	statusList = list(StatusInput.objects.all())
	statusList.reverse()
	response['result'] = statusList
	response['form'] = StatusForms
	return render(request, 'Story6.html', response)

def lab6_profile(request):
	return render(request, "Story6prof.html")

def add_result(request):
	form = StatusForms(request.POST)
	if request.method == 'POST' and form.is_valid():
		data = form.cleaned_data
		StatusInput.objects.create(**data)
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')