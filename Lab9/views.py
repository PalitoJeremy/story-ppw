from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import logout
import requests
import json

# Create your views here.
def story9_view(request):
	return render(request, "Story9.html")

def data(request):
	json_read = requests.get('https://enterkomputer.com/api/product/notebook.json').json()
	return JsonResponse(json_read, safe=False)

def logoutPage(request):
	request.session.flush()
	logout(request)
	return render(request, "Story9.html")
