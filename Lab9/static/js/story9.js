$(document).ready(function() {
		$.ajax({
			type: "GET",
			url: "data",
			datatype: 'json',
			success: function(data){
				$('tbody').html('')
				var result ='<tr>';
				for(var i = 0; i < 10; i++) {
					result += data+"<td class='align-middle'>" + data[i].name +"</td>" +
					"<td class='align-middle'>" + data[i].brand_description + "</td>" + 
					"<td class='align-middle' id='product"+ i +"'>" + data[i].price +"</td>" + 
					"<td class='align-middle'><button class='btn btn-default' id='button"+ i +"' onclick='wishlist(product" + i +");changeColor(button" + i +")'><b>+</b></button></td></tr>"
				}
				$('tbody').append(result);
			},
			error: function(error){
				$('#alert').html('')
				var result = "Server error :(";
				$('#alert').append(result);
			}
			
		});
});
var counter = 0;
function wishlist(clicked_id){
	var btn = clicked_id;	
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		counter-= parseInt(clicked_id.innerHTML);
		document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		counter+= parseInt(clicked_id.innerHTML);
		document.getElementById("counter").innerHTML = counter;
	}
}

function changeColor(clicked_id){
	var btn = clicked_id;	
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked", "btn-success");
		btn.classList.add("btn-default");
	}
	else{
		btn.classList.remove("btn-default");
		btn.classList.add("checked", "btn-success");
	}
}


