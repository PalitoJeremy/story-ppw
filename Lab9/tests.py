from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import story9_view
from .views import data
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class Lab9UnitTest(TestCase):
	def test_lab9_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab9_using_lab9_func(self):
		found = resolve('/')
		self.assertEqual(found.func, story9_view)

	def test_lab9_using_data_func(self):
		found = resolve('/data/')
		self.assertEqual(found.func, data)